// 所有模块都通过 define 来定义
define(function(require, exports, module) {

	var $ = require('$');

	//判断是否数组
	function isArray(obj){
		return Object.prototype.toString.call(obj) === '[object Array]';   
	}

	//判断数组中是否有重复数据
	function isRepeat(arr){
	  var hash = {};
	  for(var i in arr) {
	    if(hash[arr[i]])
	    	return true;
	    hash[arr[i]] = true;
	  }
	  return false;
	}

	//空 空字符 undefined
	function isNone(param){
		return param==undefined || param ==null || param =="";
	}

	module.exports = {
		isArray: isArray,
		isRepeat:isRepeat,
		isNone:isNone
	}


});