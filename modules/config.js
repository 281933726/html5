seajs.config({
	  base: "../modules/",
	  alias: {
	    "$":"frozenjs/lib/zepto.min.js?_bid=304",
        "frozen":"frozenjs/1.0.1/frozen.js?_bid=304",
	    "helper": "helper/helper.js",
	    "mobilebone": "mobilebone/src/mobilebone.js"
	  }
});