 // 所有模块都通过 define 来定义
define(function(require, exports, module) {

	// 通过 require 引入依赖
	var $ = require('$');//../modules/frozenjs/lib/zepto.min.js?_bid=304
	var fz = require('frozen');
	var helper = require('helper');
	

	//初始化
	var _init = function(){

	    var slider =new fz('.ui-slider', {
	        role: 'slider',
	        indicator: true,
	        autoplay: true,
	        interval: 3000
	    });

	    
	}


	window.addEventListener('load', function(){		    
		_init();
	})
	
	
	exports.init = _init;

	
	
	 

});